<!DOCTYPE html>
<html>
  <head>
  	<!-- xxx Basics xxx -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- xxx Change With Your Information xxx -->    
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />

    <!-- TITLE + SEO + FAVICON -->
    <?php include("includes/title_seo_favicon.php"); ?>

    <!-- Core Css Stylesheets -->
    <link href="<?= $path; ?>css/base.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <body>
    	<!-- xxx Loader Start xxx -->    
    <div id="pageloader">   
        <div class="loader-item">
          <img src="<?= $path; ?>images/site-loader.gif" alt='loader' />
          <div>loading...</div>
        </div>
    </div>
    <!-- xxx End xxx -->
        <!-- xxx Home Section xxx -->
        <section>
            <header>
                    <div class="logo">
                        <a href="#"><img src="<?= $path; ?>images/logo.png" alt="Koida Logo"></a>
                    </div>
                    <nav class="navbar navbar-default">            
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="index-default.html#home">Home</a></li>
                                <li><a href="index-default.html#about-us">About Us</a></li>
                                <li><a href="index-default.html#services">Services</a></li>
                                <li><a href="index-default.html#gallery">Gallery</a></li>
                                <li><a href="index-default.html#packages">Packages</a></li>
                                <li><a href="index-default.html#team">Team</a></li>
                                <li><a href="index-default.html#clients">Clients</a></li>       
                                <li class="active"><a href="index-default.html#blog">Blog</a></li>                               
                                <li><a href="index-default.html#contact">Contact</a></li>
                            </ul>
                        </div>            
                    </nav>
                </header>            
        </section>
        <!-- xxx Home Section xxx -->
        
        <!-- xxx Blog Section xxx -->
        <section id="blog" class="body-content">        	
            <div class="container">
            	<div class="row">
                    
                  	<div class="col-sm-8">
                    <div class="main-heading blog-main-title">
                            <h1>Blog Single</h1>
                            <h4>Life Style</h4>
                        </div>
                    	<div class="blog-box2">
                            <img src="<?= $path; ?>images/blog/single-1.jpg" alt="">
                            <div class="meta-box">
                                <div class="blog-icon">
                                    <a href="#"><i class="fa fa-comments-o"></i> 12</a>
                                </div>
                                <div class="blog-head">
                                    <h3><a href="#">Our New Spa</a></h3>
                                    Posted by <a href="#">Admin</a>
                                </div>
                            </div>
                            <div class="blog-posttext">
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Aliquam gravida, urna quis ornare imperdiet, urna lacus egestas massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                                <p>Vivamus imperdiet pulvinar risus, at posuere justo scelerisque sed. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Praesent pellentesque diam vitae nibh aliquam faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
                                <p><a href="#">Fusce gravida quam non velit ornare bibendum.</a> Etiam sit amet diam arcu. Nunc tristique tellus quam, id ultrices urna rhoncus non. Cras rhoncus interdum arcu eget congue. Curabitur non justo velit.</p>
                            </div>
                            
                            <div class="social-icons text-center">
                                <ul>
                                    <li><a href="#" class="img-circle" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="img-circle" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="img-circle" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="img-circle" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#" class="img-circle" data-toggle="tooltip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                            
                            <div class="blog-post-wrap">
                                <div class="team-box">
                                    <div class="team-text">
                                        <div class="team-thumb img-circle">
                                            <img src="<?= $path; ?>images/team/testimonial-1.jpg" alt="">
                                        </div>
                                        <h3>Brand Fashkin</h3>
                                       	<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Aliquam gravida, urna quis ornare imperdiet, urna lacus egestas massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit..</p>
                                        <div class="clearfix">&nbsp;</div>
                                        <a href="#" class="main-btn">View Profile</a>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="comment-box">
                            	<h3>Recent Comments</h3>
                                <div class="comment-text">
                                	<div class="comment-thumb">
                                    	<div class="thumb">
                                    		<img src="<?= $path; ?>images/team/team-2.jpg" alt="" class="img-circle">
                                        </div>
                                        <div class="comment-head">
                                            <h5>Brad Thomos</h5>
                                            <strong>03-Sep-2014</strong>
                                        </div>
                                    </div>                                	
                                	<p>Aliquam adipiscing tincidunt lectus, eget adipiscing magna auctor eu. Quisque rhoncus sollicitudin massa non tempor. Cras sit amet mi ut erat rhoncus adipiscing at vel augue.</p>	
                                    <p class="text-right"><a class="main-btn" href="#">Reply &nbsp;<i class="fa fa-reply"></i></a></p>
                                </div>
                                
                                <div class="comment-text reply">
                                	<div class="comment-thumb">
                                    	<div class="thumb">
                                    		<img src="<?= $path; ?>images/team/team-1.jpg" alt="" class="img-circle">
                                        </div>
                                        <div class="comment-head">
                                            <h5>Brad Thomos</h5>
                                            <strong>03-Sep-2014</strong>
                                        </div>
                                    </div>                                	
                                	<p>Aliquam adipiscing tincidunt lectus, eget adipiscing magna auctor eu. Quisque rhoncus sollicitudin massa non tempor. Cras sit amet mi ut erat rhoncus adipiscing at vel augue.</p>	
                                    <p class="text-right"><a class="main-btn" href="#">Reply &nbsp;<i class="fa fa-reply"></i></a></p>
                                </div>
                                
                                <div class="comment-text">
                                	<div class="comment-thumb">
                                    	<div class="thumb">
                                    		<img src="<?= $path; ?>images/team/team-4.jpg" alt="" class="img-circle">
                                        </div>
                                        <div class="comment-head">
                                            <h5>Brad Thomos</h5>
                                            <strong>03-Sep-2014</strong>
                                        </div>
                                    </div>                                	
                                	<p>Aliquam adipiscing tincidunt lectus, eget adipiscing magna auctor eu. Quisque rhoncus sollicitudin massa non tempor. Cras sit amet mi ut erat rhoncus adipiscing at vel augue.</p>	
                                    <p class="text-right"><a class="main-btn" href="#">Reply &nbsp;<i class="fa fa-reply"></i></a></p>
                                </div>
                            </div>
                            
                            <div class="comment-form">
                            	<h3>Add Comments</h3>
                                <div class="row">
                                    <form action="#" method="post" id="comment-form" novalidate="novalidate">
                                        <div class="col-sm-6">
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="col-sm-12">
                                            <textarea name="comment" id="comment" class="form-control" rows="4" placeholder="Message"></textarea>
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="form-btn" type="submit">Submit Comment</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                    	<div class="widget-box">
                            <div class="input-group col-sm-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search Here">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>Latest Posts</h3>
                            
                            <ul class="list-unstyled post-list">
                            	<li>
                                	<div class="post-thumb">
                                    	<img src="<?= $path; ?>images/blog/blog-2.jpg" alt="">
                                    </div>
                                    <div class="post-text">
                                    	<h4><a href="#">At Beach After Spa</a></h4>
                                        <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                                        <p>Nunc sit amet nisl ut ipsum auctor placerat ut at turpis. </p>
                                        <a href="#">more <i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </li>
                            	<li>
                                	<div class="post-thumb">
                                    	<img src="<?= $path; ?>images/blog/blog-3.jpg" alt="">
                                    </div>
                                    <div class="post-text">
                                    	<h4><a href="#">Additional Services</a></h4>
                                        <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                                        <p>Nunc sit amet nisl ut ipsum auctor placerat ut at turpis. </p>
                                        <a href="#">more <i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </li>
                                <li>
                                	<div class="post-thumb">
                                    	<img src="<?= $path; ?>images/blog/blog-4.jpg" alt="">
                                    </div>
                                    <div class="post-text">
                                    	<h4><a href="#">Certified Products</a></h4>
                                        <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                                        <p>Nunc sit amet nisl ut ipsum auctor placerat ut at turpis. </p>
                                        <a href="#">more <i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>Categories</h3>                            
                            <ul class="fa-ul list-dark list-unstyled">
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">Photography</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">Web Design</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">Automation</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">Google Analytic</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">Online Marketing</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">WordPress Development</a></li>
                            </ul>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>Popular Tags</h3>                            
                            <div class="tags">
                                <a href="#">Music</a>
                                <a href="#">New</a>
                                <a href="#">Outstanding</a>
                                <a href="#">Quote</a>
                                <a href="#">Web</a>
                                <a href="#">Shortcode</a>
                                <a href="#">WordPress</a>
                            </div>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>Archives</h3>                            
                            <ul class="fa-ul list-dark list-unstyled">
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">February 2014</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">March 2014</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">April 2014</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">May 2014</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">June 2014</a></li>
                                <li><i class="fa-li fa fa-angle-double-right"></i><a href="#">July 2014</a></li>
                            </ul>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>From Flickr</h3>
                            <ul id="basicuse" class="photo-thumbs"></ul>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="widget-box">
                        	<h3>About Us</h3>                            
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- xxx Blog Section xxx -->
        
        <section class="copyright home-pad">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">                  
                        <a href="http://koida.net" target="_blank"> Koi&amp;da</a> © 2017. All Rights Reserved.
                    </div>
                    <div class="col-sm-8">
                          Landing Page Template Designed &amp; Developed By: <a href="http://koida.net" target="_blank">Koida</a>
                    </div>
                </div>
            </div>
        </section>
        
		<!-- xxx Back To Top xxx -->
        <div id="back-top">
            <a class="img-circle" href="#top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
        <!-- xxx End xxx -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script type='text/javascript' src="<?= $path; ?>js/jquery-min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/bootstrap.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/smooth-scroll.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/jquery.cubeportfolio.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/gallery-custom.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/owl.carousel.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/jquery.validate.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/wow.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/vegas.min.js"></script>
    <script type="text/javascript" src="<?= $path; ?>js/letter-animation.js"></script>
    
    <!-- JQuery Map Plugin -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="<?= $path; ?>js/jquery.gmap.min.js"></script>    
    
    <script type='text/javascript' src="<?= $path; ?>js/site-custom.js"></script>
    
    <script type="text/javascript">		
		wow = new WOW(
		  {
			animateClass: 'animated',
			offset:       100
		  }
		);
		wow.init();		
	</script>   
    
    </body>
</html>
