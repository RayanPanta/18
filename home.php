<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
   	<!-- xxx Change With Your Information xxx -->    
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />

    <!-- TITLE + SEO + FAVICON -->
    <?php include("includes/title_seo_favicon.php"); ?>

    <!-- Core Css Stylesheets -->
    <link href="<?= $path; ?>css/base.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<!-- xxx Loader Start xxx -->    
    <div id="pageloader">   
        <div class="loader-item">
          <img src="<?= $path; ?>images/site-loader.gif" alt='loader' />
          <div>loading...</div>
        </div>
    </div>
    <!-- xxx End xxx -->
	
    <!-- Main Header Start -->
    <section id="home" class="bg-none">
        <!--<div class="page-overlay"></div>-->
        <div class="container">         
            <div class="home-header">
                <header>
                    <div class="logo">
                        <a href="#"><img src="<?= $path; ?>images/logo.png" alt="Koida Logo"></a>
                    </div>
                    <nav class="navbar navbar-default">            
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#home">Home</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#welcome-wrap">About Us</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#services">Services</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#gallery">Gallery</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#packages">Packages</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#team">Team</a></li>
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#clients">Clients</a></li>       
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#blog">Blog</a></li>                                       
                                <li><a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact">Contact</a></li>
                            </ul>
                        </div>            
                    </nav>
                </header>
                <div class="header-text">
                    <h1>Complete Care Under <br>One Roof Of <span>Koida</span></h1>
                </div>
                <div class="text-center arrow-down">
                    <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#welcome-wrap"><i class="fa fa-angle-double-down"></i></a>
                </div>
                
            </div>            
        </div>
    </section>
    <!-- Main Header End -->     
    
    <!-- Main Bodycontent Start -->
    <div id="body-content">
        <!-- Welcome Start -->
    	<section id="welcome-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0">
                        <img src="<?= $path; ?>images/welcom-spa.jpg" alt="Koi And Spa | Responsive HTML Template">
                    </div>
                    <div class="col-md-7 col-sm-12 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0">
                    	<div class="welcome-text">
                            <h1>Welcome To <span>Koi&amp;da</span></h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in, ultriciesnulla. Donec in urna sem. Nulla facilisiestibulum ut aliquet agna. </p><br>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque lla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                        </div>
                    </div>
                </div>
                
                <div class="row home-services">
                	<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0" data-wow-delay="0s">
                    	<div class="home-services-wrap">
                        	<div class="pull-left"><img src="<?= $path; ?>images/beauty-center.jpg" alt="" class="img-circle"></div>
                            <div class="text">
                                <h3>Beauty Center</h3>
                                <div>Pellentesque lla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0" data-wow-delay="0s">
                    	<div class="home-services-wrap">
                        	<div class="pull-left"><img src="<?= $path; ?>images/spa-center.jpg" alt="" class="img-circle"></div>
                            <div class="text">
                                <h3>Spa Canter</h3>
                                <div>Pellentesque lla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="0" data-wow-delay="0s">
                    	<div class="home-services-wrap">
                        	<div class="pull-left"><img src="<?= $path; ?>images/hair-saloon.jpg" alt="" class="img-circle"></div>
                            <div class="text">
                                <h3>Hair Salon</h3>
                                <div>Pellentesque lla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Welcome End -->
        
        <section id="services" class="home-pad">
        	<div class="container">
            	<div class="main-heading">
                	Our Services<span><i class="fa fa-leaf"></i></span>
                </div>
                <!-- Services Start -->
                <div class="owl-carousel wow bounceInUp" data-wow-duration="0" data-wow-delay="0" id="home-servies">
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-1.jpg" alt="" class="img-circle">
                            <h3>Stone Massage</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-2.jpg" alt="" class="img-circle">
                            <h3>Luxury Facial</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-3.jpg" alt="" class="img-circle">
                            <h3>Body Massage</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-4.jpg" alt="" class="img-circle">
                            <h3>Pedicure</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-5.jpg" alt="" class="img-circle">
                            <h3>Manicure</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="services-wrap">
                        	<img src="<?= $path; ?>images/service-6.jpg" alt="" class="img-circle">
                            <h3>Dazzling Makeup</h3>
                            <p>Whether you are looking for a design refresh we are confident you will be pleased with the results. Integer sollicitudin vulputate lacus, congue vulputate nisl eleifend in. </p>
                        </div>
                    </div>
                </div>                
                <!-- Services End -->
            </div>
            
            <!-- Monthly Offer Start -->
            <div class="monthly-offer">
            	<div class="page-overlay"></div>
            	<div class="container">
                	<div class="row">                    
                        <div class="col-md-8 col-sm-12 col-md-offset-2">
                            <div class="col-md-4 col-sm-4 text-center offer-img wow fadeInUp" data-wow-duration="0" data-wow-delay="0s">
                                <img src="<?= $path; ?>images/monthly-offer-img.jpg" alt="" class="img-circle">
                            </div>
                            <div class="col-md-8 col-sm-8">
                            	<h2>Monthly Offer</h2>
                                <h3>Hair Rebonding</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi.</p><br>                                
                                <a href="#" class="btn btn-danger btn-lg">Only -$999 </a>
                            </div>
                            <!--Signup Form -->
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Monthly Offer End -->
            
            <!-- Features Start -->
            <div class="features-wrap">
            	<div class="main-heading">
                	Few Awesome Benefits
                    <span><i class="fa fa-leaf"></i></span>
                </div>
                
                <div class="container">
                	<div class="row">
                    	<div class="col-md-7 col-sm-6 wow fadeInLeft" data-wow-duration="0" data-wow-delay="0s">
                        	<h3>Find Your Soul Here</h3>
                            <p>Stet clita kasd gubergren, no sea takimata sanctus. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata.Stet clita kasd gubergren, no sea takimata sanctus. At vero eos et accusam et justo duo dolores et ea rebum.</p><br>
                      	</div>
                        <div class="col-md-7 col-sm-6 wow fadeInRight" data-wow-duration="0" data-wow-delay="0.2s">
                            <h3>Other Amenities</h3>
                            <ul class="fa-ul list-colored">
                                <li><i class="fa-li fa fa-cutlery"></i>Lorem ipsum dolor sit amet</li>
                                <li><i class="fa-li fa fa-heartbeat"></i>Suspendisse tristique velit vel ligula</li>
                                <li><i class="fa-li fa fa-beer"></i>Curabitur tincidunt iaculis est posuere</li>
                                <li><i class="fa-li fa fa-car"></i>Vestibulum vitae nibh elit phasellus sed</li>
                            </ul>
                        </div>
                    </div>
                </div>
            	
            </div>
            <!-- Features End -->
        </section>
        
        <!-- Gallery Start -->
        <section id="gallery" class="home-pad">    
            <?php 
                $section_id = 6;
                $r = query( 'preambles' );

                while ( $content = mysqli_fetch_assoc( $r ) ) {
                    ?>    	
                    <div class="main-heading">
                        <?= $content["heading_$lang"]; ?>
                        <span><i class="fa fa-leaf"></i></span>
                    </div>
                    <?
                }
            ?>
            <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                    All <div class="cbp-filter-counter"></div>
                </div>

                <?php 
                    $section_id = 6;
                    $r = query( 'categories' );

                    while ( $content = mysqli_fetch_assoc( $r ) ) {
                        
                        ?>
                        <div data-filter=".filter-<?= $content['id']; ?>" class="cbp-filter-item">
                            <?= $content["category_$lang"]; ?> <div class="cbp-filter-counter"></div>
                        </div>
                        <?
                    }
                ?>

            </div>
            
            <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat wow fadeIn" data-wow-duration="0" data-wow-delay="0">
                <?php 
                
                
                    $r = query('gallery');

                    while ( $content = mysqli_fetch_assoc( $r ) ) {

                        $file = "img/$site_id/".$content['img'];

                        list( $width, $height ) = getimagesize("$file");
                        
                        if ( $width > $height ) {

                            resize_img( $content['img'], 760, 380 );

                        } elseif ( $width < $height ) {

                            resize_img( $content['img'], 380, 'proportional');

                        } elseif ( $width == $height ) {

                            resize_img( $content['img'], 380, 380);

                        } 

                        ?>
                        <div class="cbp-item filter-<?= $content['category_id']; ?>">
                            <a href="img/<?= $site_id; ?>/<?= $content['img']; ?>" class="cbp-caption cbp-lightbox" data-title="Gallery Title">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="img/<?= $site_id; ?>/resize_1_<?= $content['img']; ?>" alt="">
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <div class="cbp-l-caption-title"><?= $content["heading_$lang"]; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?
                    }
                ?>
            </div>
            
            <div id="js-loadMore-mosaic-flat" class="cbp-l-loadMore-bgbutton">
                <a href="gallery/loadMore.html" class="cbp-l-loadMore-link" rel="nofollow">
                    <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                    <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                    <span class="cbp-l-loadMore-noMoreLoading">No More Images</span>
                </a>
            </div>
        </section>
        <!-- Gallery End -->
        
 		<!-- Hot Package Start -->
        <section id="packages" class="home-pad">        	
            <div class="main-heading">
                Hot Selling Packages
                <span><i class="fa fa-leaf"></i></span>
            </div>
            
            <div class="container">
            	<div class="owl-carousel wow fadeInUp" data-wow-duration="0" data-wow-delay="0" id="home-package">                    	
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Stone Massage</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Luxury Facial</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Body Massage</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Pedicure</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Manicure</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="package-wrap">
                            <div class="package-price img-circle">
                                299<span><i class="fa fa-dollar"></i></span>
                            </div>
                            <h4>Dazzling Makeup</h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>                                
                            <a data-scroll data-speed="2000" data-easing="easeOutQuint" data-url="false" href="#contact" class="btn-default btn">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
      	</section>
        <!-- Hot Package Start -->
        
        <!-- Our Team Start -->
        <section id="team" class="home-pad">        	
            <div class="main-heading">
                Our Team Members
                <span><i class="fa fa-leaf"></i></span>
            </div>
            
            <div class="container">
            	<div class="row">
                    
                    <?php 
                        //QUERY FUNCTION
                        $section_id = 2;
                        $r = query('team');
                        
                        while ( $content = mysqli_fetch_assoc( $r ) ) {                  

                            //RESIZE IMAGE                          
                            crop_img( $content['img'], 270, 270 );

                            ?>
                            <div class="col-md-3 col-sm-6 wow flipInX" data-wow-duration="0" data-wow-delay="0s">
                                <div class="team-wrap">
                                    <figure class="snip1210">
                                        <img src="img/<?= $site_id; ?>/crop_1_<?= $content['img']; ?>" alt=""/>
                                      <figcaption>
                                        <a href="#"><span><i class="fa fa-twitter"></i></span></a>
                                        <a href="#"><span><i class="fa fa-facebook"></i></span></a>
                                        <a href="#"><span><i class="fa fa-google-plus"></i></span></a>
                                      </figcaption>
                                    </figure>
                                    <div class="text">
                                        <h3><?= $content['name']; ?> <?= $content['last_name']; ?></h3>
                                        <h5><?= $content["position_$lang"]; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                    ?>

                </div>
            </div>
      	</section>
        <!-- Our Team End -->
        
        <!-- Our Client Start -->
        <section id="clients" class="home-pad">        	
            <div class="main-heading">
                Client's Testimonials
                <span><i class="fa fa-leaf"></i></span>
            </div>
            
            <div class="container">
            	<div class="row">
                	<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 wow fadeInDown" data-wow-duration="0" data-wow-delay="0.1s">
                        <div class="owl-carousel" id="home-testimonial">
                            <div class="item">
                                <div class="testimonial-wrap">
                                	<img src="<?= $path; ?>images/team/testimonial-1.jpg" class="img-circle" alt="">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Aliquam gravida, urna quis ornare imperdiet, urna lacus egestas massa. </p>
                                    <h3>- John Gerry  <span>Design Hunt</span></h3>
                                    
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-wrap">
                                	<img src="<?= $path; ?>images/team/testimonial-2.jpg" class="img-circle" alt="">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Aliquam gravida, urna quis ornare imperdiet, urna lacus egestas massa. </p>
                                    <h3>- John Gerry  <span>Design Hunt</span></h3>
                                    
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-wrap">
                                	<img src="<?= $path; ?>images/team/testimonial-3.jpg" class="img-circle" alt="">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Aliquam gravida, urna quis ornare imperdiet, urna lacus egestas massa. </p>
                                    <h3>- John Gerry  <span>Design Hunt</span></h3>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                	<div class="owl-carousel wow bounceInUp" data-wow-duration="0" data-wow-delay="0.2s" id="home-clients">
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client1.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client2.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client3.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client4.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client5.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client6.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client1.png" alt="">                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-wrap">
                                <img src="<?= $path; ?>images/clients/client2.png" alt="">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="offers-wrap">
            	<div class="container">
                	<div class="main-heading">
                        Amazing Offers
                        <span><i class="fa fa-leaf"></i></span>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="0" data-wow-delay="0">
                        	<h4 class="text-center">Subscribe to our newsletter for special offers</h4>
                        	<div class="row">
                            	<div class="col-sm-3">
                                	<div class="offer-icon img-circle">
                                    	<i class="fa fa-gift"></i>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                	<form class="form-inline">
                                        <div class="form-group">
                                            <input id="subscribe-email" placeholder="Email Address" name="subscribe-email" class="col-xs-12">
                                        </div>
                                        <button type="submit" id="subscribe-submit" class="col-xs-push-5 btn btn-default">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                 	</div>
           		</div>
            </div>
      	</section>
        <!-- Our Client End -->
        
        <!-- Recent Blog Start -->
 		<section id="blog" class="home-pad">        	
            <div class="main-heading">
                Recent Blog
                <span><i class="fa fa-leaf"></i></span>
            </div>
            
            <div class="container">            	
                <div class="row">
                	<div class="col-sm-3">
                    	<div class="blog-wrap">
                            <div class="blog-wrap-img img-circle">
                                <img src="<?= $path; ?>images/blog/blog-1.jpg" class="" alt="">
                            </div>
                            <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                            <h4><a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>">Our Spa</a></h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>" class="btn-default btn">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    	<div class="blog-wrap">
                            <div class="blog-wrap-img img-circle">
                                <img src="<?= $path; ?>images/blog/blog-2.jpg" class="" alt="">
                            </div>
                            <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                            <h4><a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>">At Beach After Spa</a></h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>" class="btn-default btn">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    	<div class="blog-wrap">
                            <div class="blog-wrap-img img-circle">
                                <img src="<?= $path; ?>images/blog/blog-3.jpg" class="" alt="">
                            </div>
                            <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                            <h4><a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>">Additional Services</a></h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>" class="btn-default btn">Read More</a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    	<div class="blog-wrap">
                            <div class="blog-wrap-img img-circle">
                                <img src="<?= $path; ?>images/blog/blog-4.jpg" class="" alt="">
                            </div>
                            <div class="meta-text"><i class="fa fa-clock-o"></i> 04.10.2013 <span>/</span> <a href="#"><i class="fa fa-tag"></i> Design</a> <span>/</span> 
                                    <a href="#"><i class="fa fa-comment-o"></i> 2</a></div>
                            <h4><a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>">Certified Products</a></h4>
                            <p>Nulla nec faucibus est. In in augue placerat, ligula quis, elementum augue.</p>
                            <a href="index.php?page=blog_content&amp;lang=<?= $lang; ?>" class="btn-default btn">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            
            
      	</section>
        <!-- Recent Blog End -->
        
        <!-- Contact Start -->
        <section id="contact" class="home-pad">        	
            <div class="main-heading">
                Contact Us
                <span><i class="fa fa-leaf"></i></span>
            </div>
            
            <div class="container">
            	<div class="row">
                	<div class="col-sm-3 wow fadeInLeft" data-wow-duration="1" data-wow-delay="0">
                    	<div class="contact-wrap">
                        	<div class="contact-icon img-circle">
                            	<i class="fa fa-map-signs"></i>
                            </div>
                        	<h3>Our Location</h3>
                            <div>Envato Pty Ltd, 13/2 Elizabeth St <br>Melbourne VIC 3000, Australia </div>
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInLeft" data-wow-duration="1" data-wow-delay="0.2s">
                    	<div class="contact-wrap">
                        	<div class="contact-icon img-circle">
                            	<i class="fa fa-phone"></i>
                            </div>
                        	<h3>Phone Number</h3>
                            <div>Phone: +88 (0) 101 0000 000 <br>Fax: +88 (0) 202 0000 001</div>
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInLeft" data-wow-duration="1" data-wow-delay="0.4s">
                    	<div class="contact-wrap">
                        	<div class="contact-icon img-circle">
                            	<i class="fa fa-envelope"></i>
                            </div>
                        	<h3>Email</h3>
                            <div><a href="#">info@wll&amp;da.com</a><br> <a href="#">support@wll&amp;da.com</a></div>
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInLeft" data-wow-duration="1" data-wow-delay="0.6s">
                    	<div class="contact-wrap">
                        	<div class="contact-icon img-circle">
                            	<i class="fa fa-clock-o"></i>
                            </div>
                        	<h3>Business Hours</h3>
                            <div>9 am to 6 pm on Weekdays <br>7 am to 9 pm on Weekends</div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                    	<section class="hs-recent-posts">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 left-content">                                    	
                            			<div class="bg-left">
                                        	<div id="map-holder">
                                                <div id="map_extended">
                                                    <p>This will be replaced with the Google Map.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7 right-content">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="row hs-content">
                                                <div class="col-sm-12 smart-mar">
                                                    <div>
                                                        <div id="sucessmessage"> </div>
                                                    </div>
                                                </div>
                                                <form action="#" method="post" id="contact_form" novalidate="novalidate">
                                                    <div class="col-sm-6">
                                                        <input type="text" name="name" id="name" class="form-control" placeholder="First Name">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <textarea name="comment" id="comment" class="form-control" rows="8" placeholder="Message"></textarea>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="form-btn">Contact Us</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- / .row -->
                            </div>
                            <!-- /.container -->
                        </section>
                    </div>
                </div>
            </div>
      	</section>
        <!-- Contact End -->
        
        
        
   	</div>
    <!-- Main Bodycontent End -->
    
    <section class="copyright home-pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">                	
                    <a href="http://koida.net" target="_blank"> Koi&amp;da</a> © 2017. All Rights Reserved.
                </div>
                <div class="col-sm-8">
                      Landing Page Template Designed &amp; Developed By: <a href="http://koida.net" target="_blank">Koida</a>
                </div>
            </div>
        </div>
    </section>
    
    <!-- xxx Back To Top xxx -->
    <div id="back-top">
        <a class="img-circle" href="#top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
    <!-- xxx End xxx -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script type='text/javascript' src="<?= $path; ?>js/jquery-min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/bootstrap.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/smooth-scroll.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/jquery.cubeportfolio.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/gallery-custom.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/owl.carousel.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/jquery.validate.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/wow.min.js"></script>
    <script type='text/javascript' src="<?= $path; ?>js/vegas.min.js"></script>
    <script type="text/javascript" src="<?= $path; ?>js/letter-animation.js"></script>
    
    <!-- JQuery Map Plugin -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="<?= $path; ?>js/jquery.gmap.min.js"></script>    
    
    <script type='text/javascript' src="<?= $path; ?>js/site-custom.js"></script>
    
    <script type="text/javascript">     
        wow = new WOW(
          {
            animateClass: 'animated',
            offset:       100
          }
        );
        wow.init(); 
        
        $("body").vegas({
            slide: 0,
            delay: 5000,
            transition: [ 'fade', 'zoomOut', 'swirlLeft' ],
            slides: [
                { src: "<?= $path; ?>images/slideshow-1.jpg" },
                { src: "<?= $path; ?>images/slideshow-2.jpg" },
                { src: "<?= $path; ?>images/slideshow-3.jpg" }
            ]
        }); 
    </script>  
</body>
</html>
